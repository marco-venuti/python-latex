#!/bin/bash

folder=$(find . -maxdepth 1 -type d -name "install-tl*")
$folder/install-tl --repository http://tug.ctan.org/systems/texlive/tlnet --profile=texlive.profile
tlmgr update --self
tlmgr install $(cat tex-pkg.txt)
tlmgr update --all
