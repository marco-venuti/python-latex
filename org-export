#!/usr/bin/env bash

set -e

function usage () {
    cat <<EOF
$(basename $0) [--html] [--twbs] [--pdf] [--file <filename>]

Options:
  --html            export to plain html
  --twbs            export to html wih bootstrap
  --pdf             export to latex and pdf
  --file            org file

EOF
    exit 0
}


HTML=0
TWBS=0
PDF=0

while [ "$1" != "" ]; do
    case $1 in
        --html ) HTML=1;;
        --twbs ) TWBS=1;;
        --pdf  ) PDF=1;;
        --file ) shift
                 FILENAME=$1;;
        --help ) usage
                 exit;;
        * ) usage
            exit;;
    esac
    shift
done

if [ ${HTML} -ne 0 ]; then
    emacs --batch -l ~/.emacs.d/init.el --file ${FILENAME} -f org-html-export-to-html
fi

if [ ${TWBS} -ne 0 ]; then
    emacs --batch -l ~/.emacs.d/init.el --file ${FILENAME} -f org-twbs-export-to-html
fi

if [ ${PDF} -ne 0 ]; then
    emacs --batch -l ~/.emacs.d/init.el --file ${FILENAME} -f org-latex-export-to-pdf
fi
