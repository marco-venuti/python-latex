(package-initialize)
(require 'package)

(add-to-list
 'package-archives
 '("melpa" . "https://melpa.org/packages/")
 )

;; Emacs 26.1 bug, needed when emacs can't download packages
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Install use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

(use-package org
  :hook (org-mode . (lambda () (add-to-list 'org-latex-compilers "latexmk")))
  :config
  (setq org-latex-compiler "latexmk"
        org-latex-pdf-process '("%latex -shell-escape -output-directory=%o %f")
        org-latex-listings 'minted
        org-latex-default-packages-alist
        '(("AUTO" "inputenc" t ("pdflatex"))
          ("T1" "fontenc" t ("pdflatex"))
          ("" "graphicx" t nil)
          ("" "grffile" t nil)
          ("" "longtable" nil nil)
          ("" "wrapfig" nil nil)
          ("" "rotating" nil nil)
          ("normalem" "ulem" t nil)
          ("" "textcomp" t nil)
          ("bookmarks,colorlinks,linkcolor=blue" "hyperref" nil nil)
          ("" "booktabs")
          ("" "enumitem")
          ("" "capt-of" nil nil)
          ("" "minted")
          ("" "amsmath" t nil)
          ("" "amssymb" t nil)
          ("" "amsfonts" t nil)
          ("" "mathrsfs")
          ("" "mathtools")
          ("" "commath")
          ("" "halloweenmath"))
        )
  (setq org-html-mathjax-template
        "<script>
    window.MathJax = {
      startup: {
        ready: () => {
          document.body.innerHTML = document.body.innerHTML.replace(/dcases/g, 'cases');
          MathJax.startup.defaultReady();
        }
      },
      svg: {
        displayAlign: \"%ALIGN\",
        displayIndent: \"%INDENT\",
        scale: %SCALE
      },
      chtml: {
        scale: %SCALE
      },
      options: {
        ignoreHtmlClass: 'tex2jax_ignore',
        processHtmlClass: 'tex2jax_process'
      },
      tex: {
        autoload: {
          color: [],
          colorV2: ['color']
        },
        packages: {'[+]': ['noerrors', 'ams', 'braket']},
        tags: 'ams',
        macros: {
          dif: ['\\\\operatorname{d} \\\\!'],
          dod: ['\\\\dfrac{\\\\dif{^{#1}}#2}{\\\\dif{#3^{#1}}}', 3, ''],
          dpd: ['\\\\dfrac{\\\\partial{^{#1}} \\\\! #2}{\\\\partial{\\\\! #3^{#1}}}', 3, ''],
          abs: ['\\\\left|#1\\\\right|', 1],
          del: ['\\\\left(#1\\\\right)', 1],
          sbr: ['\\\\left[#1\\\\right]', 1],
          cbr: ['\\\\left\\\\{#1\\\\right\\\\}', 1],
          coloneqq: ['\\\\mathrel{\\\\vcenter{:}}='],
          eqqcolon: ['=\\\\mathrel{\\\\vcenter{:}}'],
          ped: ['_{\\\\text{#1}}', 1],
          ap: ['^{\\\\text{#1}}', 1],
          N: ['\\\\mathbb{N}'],
          R: ['\\\\mathbb{R}'],
          C: ['\\\\mathbb{C}'],
        }
      },
      loader: {
        load: ['[tex]/noerrors', '[tex]/ams', '[tex]/braket']
        }
      };
  </script>
  <script id=\"MathJax-script\" async src=\"%PATH\"></script>
"
        )
  (setq org-html-mathjax-options
        '(
          (path "https://cdn.jsdelivr.net/npm/mathjax@3.0.1/es5/tex-svg.js")
          (scale "1")
          (dscale "1")
          (align "center")
          (indent "2em")
          (messages "none")
          )
        )
  )

(use-package ox-twbs
  :ensure t
  :config
  (setq org-twbs-mathjax-template org-html-mathjax-template
        org-twbs-mathjax-options org-html-mathjax-options)
  )

(use-package htmlize :ensure t)
