![build status](https://gitlab.com/marco-venuti/python-latex/badges/master/pipeline.svg)

# Python Latex Docker image

## Immagine con Emacs
Contiene un'installazione di `emacs-nox` con una configurazione minimale della `org-mode` per l'export da org verso
- $`\LaTeX`$ e quindi `pdf`
- `html` plain con `mathjax` configurato
- `html` con twitter-bootrstrap (`ox-twbs`) con `mathjax` configurato

Per dettagli sulla configurazione di `mathjax` vedi il file `init.el`.

È inoltre presente uno script che implementa una funzione stupida che faccia da alias al seguente comando di esempio per l'export da riga di comando
``` bash
emacs --batch -l ~/.emacs.d/init.el --file org-file.org -f org-latex-export-to-pdf
```
L'utilizzo del comando è il seguente
``` bash
org-export [--html] [--twbs] [--pdf] [--file <filename>]

Options:
  --html            export to plain html
  --twbs            export to html wih bootstrap
  --pdf             export to latex and pdf
  --file            org file
```
